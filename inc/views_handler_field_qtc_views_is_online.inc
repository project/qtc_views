<?php

/**
 * @file views_handler_field_qtc_views_is_online.inc
 *
 * QTChat Views is user online handler.
 */

/**
 * Is user online handler. Useful for newbies who don't know how to do this
 * using filters.
 */
class views_handler_field_qtc_views_is_online extends views_handler_field_boolean {
  function query() {
    $time_diff = variable_get('qtc_online_status_time', 900);
    $sql = "(". time() ." - users.access) < ". $time_diff;
    // We liberally steal from views_handler_sort_formula here.
    $this->ensure_my_table();
    $this->field_alias = $this->query->add_field(NULL, $sql, $this->table_alias .'_'. $this->field);
    $this->query->add_where($this->options['group'], 'users.uid <> 0');
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['type'] = array('default' => 'online-offline');
    $options['time_info'] = array();

    return $options;
  }

  /**
   * Add the online-offline type to options form.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['type']['#options']['online-offline'] = t('Online/Offline');

    $online_time = variable_get('qtc_online_status_time', 900);
    $time_options = _qtc_online_time_options();

    $form['time_info'] = array(
      '#type' => 'markup',
      '#value' => '<div class="form-item">'. t('Currently defined offline time: !offline-time', array('!offline-time' => $time_options[$online_time])) .'</div>',
    );
  }

  function render($values) {
    $value = $values->{$this->field_alias};
    if (!empty($this->options['not'])) {
      $value = !$value;
    }
    if ($this->options['type'] == 'online-offline') {
      return $value ? '<span class="user-stat-online">'. t('Online') .'</span>' : '<span class="user-stat-offline">'. t('Offline') .'</span>';
    }
    else {
      return parent::render($values);
    }
  }
}
