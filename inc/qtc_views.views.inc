<?php

/**
 * @file qtc_views.views.inc
 *
 * Provides the Views module integration with qtc_views.module.
 */

/**
 * Implementation of hook_views_data()
 */
function qtc_views_views_data() {
  // Link to invite user to chat. (virtual field)
  $data['users']['chat_to'] = array(
    'group' => t('QTChat'),
    'title' => t('Chat link'),
    'help' => t('Provide an chat link to chat with an user.'),
    'real_field' => 'access',
    'field' => array(
      'handler' => 'views_handler_field_qtc_views_chat_link',
    ),
  );
  // Is user online. boolean, (virtual)
  $data['users']['user_is_online'] = array(
    'group' => t('QTChat'),
    'title' => t('User is online'),
    'help' => t('Shows whether the user is online.'),
    'real_field' => 'access',
    'field' => array(
      'handler' => 'views_handler_field_qtc_views_is_online',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_qtc_views_is_online',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_qtc_views_is_online',
    ),
  );
  // All users online counter. numeric, (virtual field)
  $data['users']['user_all_online_counter'] = array(
    'group' => t('QTChat'),
    'title' => t('All user online counter'),
    'help' => t('Count the number of all users by their online status. Note: Use this field with no arguments or relationships.'),
    'real_field' => 'access',
    'field' => array(
      'handler' => 'views_handler_field_qtc_views_user_all_online_counter',
    ),
  );

  // Flag Friend; Friend counter. numeric, (virtual field)
  $data['users']['flag_friend_counter'] = array(
    'group' => t('QTChat'),
    'title' => t('Flag Friend: Friend counter'),
    'help' => t('Count the number of friends.'),
    //'real_field' => 'access',
    'field' => array(
      'handler' => 'views_handler_field_qtc_views_flag_friend_counter',
    ),
  );

  // Flag Friend; Friend counter. numeric, (virtual field)
  $data['users']['flag_friend_online_user_counter'] = array(
    'group' => t('QTChat'),
    'title' => t('Flag Friend: Friends online counter'),
    'help' => t('Count the number of online friends.'),
    //'real_field' => 'access',
    'field' => array(
      'handler' => 'views_handler_field_qtc_views_flag_friend_online_user_counter',
    ),
  );

  return $data;
}

/**
 * Implementation of hook_views_handlers().
 */
function qtc_views_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'qtc_views') . '/inc'
    ),
    'handlers' => array(
      'views_handler_field_qtc_views_chat_link' => array(
        'parent' => 'views_handler_field',
      ),
      'views_handler_field_qtc_views_is_online' => array(
        'parent' => 'views_handler_field_boolean',
      ),
      'views_handler_sort_qtc_views_is_online' => array(
        'parent' => 'views_handler_sort',
      ),
      'views_handler_filter_qtc_views_is_online' => array(
        'parent' => 'views_handler_filter_boolean_operator',
      ),
      'views_handler_field_qtc_views_user_all_online_counter' => array(
        'parent' => 'views_handler_field_numeric',
      ),
      'views_handler_field_qtc_views_flag_friend_counter' => array(
        'parent' => 'views_handler_field_numeric',
      ),
      'views_handler_field_qtc_views_flag_friend_online_user_counter' => array(
        'parent' => 'views_handler_field_numeric',
      ),
    ),
  );
}
