<?php

/**
 * @file views_handler_filter_qtc_views_is_online.inc
 *
 * QTChat Views is user online filter handler.
 */

/**
 * Is user online sort handler. Useful for newbies who don't know how to do this
 * using filters.
 */
class views_handler_filter_qtc_views_is_online extends views_handler_filter_boolean_operator {
  function query() {
    $time_diff = variable_get('qtc_online_status_time', 900);
    $sql = "(". time() ." - users.access) < ". $time_diff;
    $this->ensure_my_table();
    $this->query->add_where($this->options['group'], $sql);
    $this->field_alias = $this->table_alias .'_'. $this->field;
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['count_info'] = array();
    $options['time_info'] = array();

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['count_info'] = array(
      '#type' => 'markup',
      '#value' => '<div class="form-item">'. t('The current user is excluded.') .'</div>',
      '#weight' => -2,
    );

    $online_time = variable_get('qtc_online_status_time', 900);
    $time_options = _qtc_online_time_options();

    $form['time_info'] = array(
      '#type' => 'markup',
      '#value' => '<div class="form-item">'. t('Currently defined offline time: !offline-time', array('!offline-time' => $time_options[$online_time])) .'</div>',
    );
  }

  /**
   * Override default True/False options.
   */
  function get_value_options() {
    $this->value_options = array(1 => t('Online'), 0 => t('Offline'));
  }
}
