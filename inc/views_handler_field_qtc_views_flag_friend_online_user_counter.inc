<?php

/**
 * @file views_handler_field_qtc_views_flag_friend_online_user_counter.inc
 *
 * QTChat Views count friends, flagged by Flag Friend.
 */

/**
 * Users by online status count handler.
 */
class views_handler_field_qtc_views_flag_friend_online_user_counter extends views_handler_field_numeric {
  function query() {
    $sql = "(".time()." - users.access) < 450 AS users_user_is_online";

    $this->query->add_field('flag_friend', 'uid');

    $this->ensure_my_table();
    
    $this->field_alias = $this->query->add_field(NULL, "". $sql ."", NULL);
    $this->query->add_where($this->options['group'], '((('. time() .' - users.access) < 450) AND (users.uid <> 0)) AND ((users.uid IN (SELECT f.friend_uid FROM {flag_friend} f WHERE f.uid = ***CURRENT_USER***)) OR (users.uid IN (SELECT f.uid FROM {flag_friend} f WHERE f.friend_uid = ***CURRENT_USER***)))');
  }

  function render($values) {
    $output = $this->options['prefix'];
    $output .= '<span class="qtc-online-counter">';
    $output .= count($this->view->result);
    $output .= '</span>';
    $output .= $this->options['suffix'];

    return $output;
  }
}
