<?php

/**
 * @file views_handler_field_qtc_views_user_all_online_counter.inc
 *
 * QTChat Views count users by online status.
 */

/**
 * Users by online status count handler.
 */
class views_handler_field_qtc_views_user_all_online_counter extends views_handler_field_numeric {
  function query() {
    $time_diff = time() - variable_get('qtc_online_status_time', 900);
    $sql = "SELECT COUNT(u.uid) FROM {users} u WHERE u.access > ". $time_diff ." AND users.uid <> ***CURRENT_USER***";
    $this->ensure_my_table();
    $this->field_alias = $this->query->add_field(NULL, "(". $sql .")", $this->table_alias .'_'. $this->field);
    $this->query->add_where($this->options['group'], 'users.uid <> ***CURRENT_USER***');
  }

  function option_definition() {
    $options = parent::option_definition();
    
    $options['count_current_user'] = array('default' => 0, 'translatable' => TRUE);
    $options['time_info'] = array();

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['count_current_user'] = array(
      '#type' => 'select',
      '#title' => 'Current user',
      '#description' => 'Counting with the current user.',
      '#options' => array(0 => 'No', 1 => 'Yes'),
      '#default_value' => $this->options['count_current_user'],
    );

    $online_time = variable_get('qtc_online_status_time', 900);
    $time_options = _qtc_online_time_options();

    $form['time_info'] = array(
      '#type' => 'markup',
      '#value' => '<div class="form-item">'. t('Currently defined offline time: !offline-time', array('!offline-time' => $time_options[$online_time])) .'</div>',
    );
  }

  function render($values) {
    if ($this->options['count_current_user'] == 0) {
      $counter = $values->users_user_all_online_counter - 1;
    }
    else {
      $counter = $values->users_user_all_online_counter;
    }

    $output = $this->options['prefix'];
    $output .= '<span class="qtc-online-counter">';
    $output .= $counter;
    $output .= '</span>';
    $output .= $this->options['suffix'];

    return $output;
  }
}
