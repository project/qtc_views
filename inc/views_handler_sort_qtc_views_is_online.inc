<?php

/**
 * @file views_handler_sort_qtc_views_is_online.inc
 *
 * QTChat Views is user online sort handler.
 */

/**
 * Is user online sort handler. Useful for newbies who don't know how to do this
 * using filters.
 */
class views_handler_sort_qtc_views_is_online extends views_handler_sort {
  function query() {
    $time_diff = variable_get('qtc_online_status_time', 900);
    $sql = "(". time() ." - users.access) < ". $time_diff;

    $this->ensure_my_table();
    $this->query->add_orderby(NULL, $sql, $this->options['order'], $this->table_alias .'_'. $this->field);
  }
}