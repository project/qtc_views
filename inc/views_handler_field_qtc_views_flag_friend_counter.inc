<?php

/**
 * @file views_handler_field_qtc_views_flag_friend_counter.inc
 *
 * QTChat Views count friends, flagged by Flag Friend.
 */

/**
 * Users by online status count handler.
 */
class views_handler_field_qtc_views_flag_friend_counter extends views_handler_field_numeric {
  function query() {
    $sql = "SELECT COUNT(*) FROM {flag_friend} ff WHERE ff.uid = users.uid OR ff.friend_uid = users.uid";

    $this->ensure_my_table();
    $this->field_alias = $this->query->add_field(NULL, "(". $sql .")", $this->table_alias .'_'. $this->field);
  }

  function render($values) {
    $output = $this->options['prefix'];
    $output .= '<span class="qtc-online-counter">';
    $output .= $values->users_flag_friend_counter;
    $output .= '</span>';
    $output .= $this->options['suffix'];

    return $output;
  }
}
