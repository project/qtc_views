<?php

/**
 * @file views_handler_field_qtc_views_chat_link.inc
 */

/**
 * Views field handler for the QTChat Views chat link.
 *
 * @ingroup views
 */
class views_handler_field_qtc_views_chat_link extends views_handler_field {
  function query() {
    if (!isset($this->view->field['name'])) {
      $this->query->add_field('users', 'name');
    }
    if (!isset($this->view->field['uid'])) {
      if ($this->view->base_table == 'node') {
        $this->query->add_field('users', 'uid');
      }
    }

    if (!isset($this->view->field['realname']) && module_exists('realname')) {
      if ($this->view->base_table == 'users') {
        $this->query->add_field('realname', 'realname');
      }
    }

    $this->query->add_field('users', 'access');
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['link_info'] = array();
    $options['link_text'] = array('default' => 1, 'translatable' => TRUE);
    $options['link_title'] = array('default' => 0, 'translatable' => TRUE);

    return $options;
  }

  /**
   * Add the link text to options form.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['link_info'] = array(
      '#type' => 'markup',
      '#value' => '<div class="form-item">'. t('The link is displayed only<br />has the corresponding user the permission <a href="!url">access qtchat</a>.', array('!url' => url('admin/user/permissions', array('fragment' => 'module-qtc')))) .'</div>',
      '#weight' => -3,
    );

    $form['link_text'] = array(
      '#type' => 'select',
      '#title' => 'Link text',
      '#description' => 'Select a link text.',
      '#options' => _views_chat_link_text_options(),
      '#default_value' => $this->options['link_text'],
      '#weight' => -2,
    );

    $title_options = array_merge(array(0 => t('No link title')), _views_chat_link_text_options());
    unset($title_options[4]);
    unset($title_options[5]);
    $form['link_title'] = array(
      '#type' => 'select',
      '#title' => 'Link title',
      '#description' => 'Select a link title. Displayed with mouse over the link.',
      '#options' => $title_options,
      '#default_value' => $this->options['link_title'],
      '#weight' => -1,
    );

    // Remove unneeded form elements.
    unset($form['empty_zero']);
  }

  function element_type() {
    return 'div';
  }

  function render($values) {
    global $user;

    if ($values->users_uid) {
      $users_uid = $values->users_uid;
    }
    if ($values->uid) {
      $users_uid = $values->uid;
    }
    
    $link = '';

    if ($user->uid != $users_uid && user_access('access qtchat', $user)) {
      $chat_user = user_load(array('uid' => $users_uid));

      if (user_access('access qtchat', $chat_user)) {
        $link_text = _views_chat_link_text_options();
        $link_title = '';

        $realname = $values->realname_realname ? $values->realname_realname : $values->users_name;

        $last = time() - $values->users_access;
        $time_diff = variable_get('qtc_online_status_time', 900);

        // Build the link text.
        $link_text = $link_text[$this->options['link_text']];
        if ($this->options['link_text'] == 4) {
          $link_text = $values->users_name;
        }
        if ($this->options['link_text'] == 5) {
          $link_text = $realname;
        }

        // Build the link title.
        if (($this->options['link_text'] == 4 || $this->options['link_text'] == 5)) {
          $link_title = array_merge(array(0 => t('No link title')), _views_chat_link_text_options());
          unset($title_options[4]);
          unset($title_options[5]);
          if ($this->options['link_title'] > 0) {
            $link_title = $link_title[$this->options['link_title']];
          }
          else {
            $link_title = '';
          }
        }
        
        // Build the chat invite link.
        if ($last < $time_diff) {
          $link = '<a title="'. $link_title .'" class="qtc-chat-invite" onclick="javascript:chatWith(\''. $users_uid .'\', \''. $values->users_name .'\', \''. $realname .'\')" href="javascript:void(0)">'. $link_text .'</a>';
        }
      }
    }

    return $link;
  }
}

/**
 * Get the link text options.
 *
 * @return array
 *   The link text options.
 */
function _views_chat_link_text_options() {
  $options = array(
           1 => t('Chat with user'),
           2 => t('Invite to chat'),
           3 => t('Chat'),
           4 => t('User name'),
         );

  if (module_exists('realname')) {
    $options_realname = array(5 => t('User realname'));
    $options = $options + $options_realname;
  }

  return $options;
}
