/* $Id:*/

Drupal.behaviors.QTCViews = function (context) {
 // Administer views field chat link.
 var linkText = $('#edit-options-link-text-wrapper');
 if (linkText.length == 1) {
  qtcSelectChange();
 }
 $('#edit-options-link-text').change(qtcSelectChange);

 function qtcSelectChange() {
  var selected = $('#edit-options-link-text option:selected');
  var output = '';
  if (selected.val() == 4 || selected.val() == 5) {
   $('#edit-options-link-title-wrapper').show();
  }
  if (selected.val() < 4) {
   $('#edit-options-link-title-wrapper').hide();
  }
 }
}
